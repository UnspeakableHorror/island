# Island Service
## Download


`git clone https://gitlab.com/UnspeakableHorror/island`

`git clone git@gitlab.com:UnspeakableHorror/island.git` 

## Build 
To build the application just run

`gradlew build`  

## Run
To run the application just run

Dev
`./gradlew bootRun --args='--spring.profiles.active=dev'`

Prod
`./gradlew bootRun`

The only difference between them is that prod executes the expiration task every 4 hours.

## Endpoints

### GET /camp/availability/from/{from}/to/{to}
Parameters 
* from: Date in YYYY-MM-DD format
* to: Date in YYYY-MM-DD format

### POST /camp/reserve
Object

Property email: A string 

Property fullName: A string

Property startDate: Date in YYYY-MM-DD format 

Property endDate: Date in YYYY-MM-DD format 

Returns the id of the reservation.

### PUT /camp/reserve
Modifies a reserve.

Object
Property id: The id of the reservation to modify.

Property email: A string 

Property fullName: A string

Property startDate: Date in YYYY-MM-DD format 

Property endDate: Date in YYYY-MM-DD format 

### DELETE /camp/reserve/{id}
Parameters
* id: The id of the reservation to cancel.
