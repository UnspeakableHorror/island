package org.homenet.uhs.reservation.db.reservation;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author uh on 2019/02/18.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class ReservationRepositoryTest {

    @Autowired
    private ReservationRepository reservationRepository;

    @Test
    public void findReservationByUserId(){
        // should find 3 for user 1 because the 2018 one is not expired here.
        List<Reservation> byUserIdAndExpiredFalse = reservationRepository.findByUserIdAndExpiredFalse(1L);

        assertFalse(byUserIdAndExpiredFalse.isEmpty());
        assertEquals(3, byUserIdAndExpiredFalse.size());
        assertEquals(1, byUserIdAndExpiredFalse.stream().map(Reservation::getUserId).distinct().count());
    }

    @Test
    public void findOverlappingReservations(){
        List<Reservation> overlappingReservations = reservationRepository.findOverlappingReservations(LocalDate.now().plusDays(7), LocalDate.now().plusDays(8));

        assertFalse(overlappingReservations.isEmpty());
        assertEquals(1, overlappingReservations.size());
    }

    @Test
    public void findAllReservations(){
        List<Reservation> allReservations = reservationRepository.findByStartDateGreaterThanAndEndDateLessThanAndExpiredFalse(LocalDate.now().minusDays(1), LocalDate.now().plusDays(11));
        assertFalse(allReservations.isEmpty());
        assertEquals(3, allReservations.size());
        assertEquals(2, allReservations.stream().map(Reservation::getUserId).distinct().count());
    }
}
