package org.homenet.uhs.reservation.service;

import org.homenet.uhs.reservation.db.reservation.Reservation;
import org.junit.Test;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author uh on 2019/02/15.
 */
public class ReservationCalendarTest {

    @Test
    public void testCalendarMarksAvailableDatesCorrectly(){
        final var start = LocalDate.of(2019, 2, 1);
        final var end = LocalDate.of(2019, 2, 15);

        var calendar = new ReservationCalendar(start, end,
                List.of(new Reservation(null, LocalDate.of(2019, 2, 2), LocalDate.of(2019, 2, 3)),
                        new Reservation(null, LocalDate.of(2019, 2, 13), LocalDate.of(2019, 6, 15))));

        assertFalse(calendar.getCalendar().isEmpty());
        assertEquals(15, calendar.getCalendar().size());

        for(ReservationCalendar.DayReserved dayReserved: calendar.getCalendar()){
            if(dayReserved.getDay().isEqual( LocalDate.of(2019, 2, 2))
                    || dayReserved.getDay().isEqual(LocalDate.of(2019, 2, 3))
                    || dayReserved.getDay().isEqual(LocalDate.of(2019, 2, 13))
                    || dayReserved.getDay().isEqual(LocalDate.of(2019, 2, 14))
                    || dayReserved.getDay().isEqual(LocalDate.of(2019, 2, 15))){
                assertTrue(dayReserved.isReserved());
            } else {
                assertFalse(dayReserved.isReserved());
            }
        }
    }
}
