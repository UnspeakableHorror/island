package org.homenet.uhs.reservation.web;

import org.homenet.uhs.Application;
import org.homenet.uhs.reservation.db.reservation.Reservation;
import org.homenet.uhs.reservation.db.reservation.ReservationRepository;
import org.homenet.uhs.reservation.db.user.User;
import org.homenet.uhs.reservation.db.user.UserRepository;
import org.homenet.uhs.reservation.service.ReservationCalendar;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.annotation.DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD;

/**
 * @author uh on 2019/02/18.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
@TestPropertySource(locations = "classpath:application-dev.properties")
// Since we are posting and deleting it might affect the other tests if we run all tests in one server.
@DirtiesContext(classMode = BEFORE_EACH_TEST_METHOD)
public class ApplicationIntegrationTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void findReservationByUserIdReturnsOnlyTwoReservations(){
        // should find 2 for user 1 because the 2018 one is expired here.
        List<Reservation> byUserIdAndExpiredFalse = reservationRepository.findByUserIdAndExpiredFalse(1L);

        assertFalse(byUserIdAndExpiredFalse.isEmpty());
        assertEquals(2, byUserIdAndExpiredFalse.size());
        assertEquals(1, byUserIdAndExpiredFalse.stream().map(Reservation::getUserId).distinct().count());
    }

    @Test
    public void getCalendarReturnsFiveReservations() {
        ResponseEntity<List<ReservationCalendar.DayReserved>> reservations = testRestTemplate.exchange("/camp/availability/from/"
                + LocalDate.now().minusDays(1)
                + "/to/" + LocalDate.now().plusMonths(1),
                HttpMethod.GET, null,
                // for some reason my version of java 11 would not compile without adding the type parameters.
                new ParameterizedTypeReference<List<ReservationCalendar.DayReserved>>() {});

        assertNotNull(reservations);
        assertNotNull(reservations.getBody());
        assertFalse(reservations.getBody().isEmpty());
        assertEquals(5, reservations.getBody().stream().filter(ReservationCalendar.DayReserved::isReserved).count());
    }

    @Test
    public void postAndModifyAndDeleteReservation(){
        var reservationDTO = new ReservationDTO();
        reservationDTO.setEmail("an@email.com");
        reservationDTO.setStartDate(LocalDate.now().plusDays(11));
        reservationDTO.setEndDate(LocalDate.now().plusDays(11));
        reservationDTO.setFullName("aName");


        // Save
        ResponseEntity<Long> id = testRestTemplate.exchange("/camp/reserve",
                HttpMethod.POST, new HttpEntity<>(reservationDTO),
                Long.class);

        assertNotNull(id);
        assertNotNull(id.getBody());

        Optional<Reservation> savedReservation = reservationRepository.findById(id.getBody());

        assertTrue(savedReservation.isPresent());

        Optional<User> userByEmail = userRepository.findByEmail(reservationDTO.getEmail());

        assertTrue(userByEmail.isPresent());

        // Modify

        reservationDTO.setId(id.getBody());
        reservationDTO.setFullName("new name");

        ResponseEntity<Long> sameId = testRestTemplate.exchange("/camp/reserve",
                HttpMethod.PUT, new HttpEntity<>(reservationDTO),
                Long.class);

        assertNotNull(sameId);
        assertNotNull(sameId.getBody());

        Optional<Reservation> modifiedReservation = reservationRepository.findById(id.getBody());

        assertTrue(modifiedReservation.isPresent());

        Optional<User> modifiedUserByEmail = userRepository.findByEmail(reservationDTO.getEmail());
        assertTrue(modifiedUserByEmail.isPresent());
        assertEquals("new name", modifiedUserByEmail.get().getFullName());


        // Delete

        testRestTemplate.delete("/camp/reserve/" + id.getBody());

        Optional<Reservation> deletedReservation = reservationRepository.findById(id.getBody());

        assertFalse(deletedReservation.isPresent());

        Optional<User> userByEmailAgain = userRepository.findByEmail(reservationDTO.getEmail());

        assertTrue(userByEmailAgain.isPresent());
    }


}
