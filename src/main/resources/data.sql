CREATE INDEX startDate ON reservation (start_date);
CREATE INDEX endDate ON reservation (end_date);

insert into user (id, email, full_name) values (1, 'example1@mail.com', 'A. User');
insert into user (id, email, full_name) values (2, 'example2@mail.com', 'B. User');

-- a reservation that will be marked as expired when the application starts.
insert into reservation (id, user_id, start_date, end_date, expired) values(1, 1, '2018-12-01', '2018-12-03', false);

-- a reservation 7 days from now.
insert into reservation (id, user_id, start_date, end_date, expired) values(2, 1, CURRENT_DATE() + 7, CURRENT_DATE() + 8, false);
insert into reservation (id, user_id, start_date, end_date, expired) values(3, 1, CURRENT_DATE() + 10, CURRENT_DATE() + 10, false);

-- a reservation that starts today.
insert into reservation (id, user_id, start_date, end_date, expired) values(4, 2, CURRENT_DATE(), CURRENT_DATE() + 1, false);
