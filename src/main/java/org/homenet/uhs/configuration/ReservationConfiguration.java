package org.homenet.uhs.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author uh on 2019/02/14.
 */
@Component
@ConfigurationProperties(prefix = "reservation")
public class ReservationConfiguration {

    private int minDays;
    private int maxDays;

    public int getMinDays() {
        return minDays;
    }

    public void setMinDays(int minDays) {
        this.minDays = minDays;
    }

    public int getMaxDays() {
        return maxDays;
    }

    public void setMaxDays(int maxDays) {
        this.maxDays = maxDays;
    }
}
