package org.homenet.uhs.errorHandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.NoSuchElementException;

/**
 * @author uh on 2019/02/12.
 */
@RestController
@ControllerAdvice
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ErrorInfo> handleIllegalArgumentException(IllegalArgumentException ex, WebRequest webRequest){
        return  new ResponseEntity<>(
                new ErrorInfo(ex.getMessage(), webRequest.getDescription(false)),
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<ErrorInfo> handleNotFoundException(Exception ex, WebRequest webRequest){
        return  new ResponseEntity<>(
                new ErrorInfo(ex.getMessage(), webRequest.getDescription(false)),
                HttpStatus.NOT_FOUND
        );
    }

    @ExceptionHandler(IllegalAccessException.class)
    public ResponseEntity<ErrorInfo> handleForbidden(Exception ex, WebRequest webRequest){
        return  new ResponseEntity<>(
                new ErrorInfo(ex.getMessage(), webRequest.getDescription(false)),
                HttpStatus.FORBIDDEN
        );
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorInfo> handleOtherException(Exception ex, WebRequest webRequest){
        return  new ResponseEntity<>(
                new ErrorInfo(ex.getMessage(), webRequest.getDescription(false)),
                HttpStatus.INTERNAL_SERVER_ERROR
        );
    }

    private class ErrorInfo {

        private final String errorDescription;
        private final String requestDescription;

        public ErrorInfo(String errorDescription, String requestDescription) {
            this.errorDescription = errorDescription;
            this.requestDescription = requestDescription;
        }

        public String getErrorDescription() {
            return errorDescription;
        }

        public String getRequestDescription() {
            return requestDescription;
        }
    }
}


