package org.homenet.uhs.reservation.db.user;


import org.springframework.util.StringUtils;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * @author uh on 2019/02/14.
 */
@Entity
public class User {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String email;

    @NotNull
    private String fullName;

    protected User(){}

    public User(String email, String fullName) {
        this.email = email;
        this.fullName = fullName;
        this.validate();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     *  Validates the user.
     * @throws IllegalArgumentException if email or name are invalid.
     */
    public void validate() throws IllegalArgumentException {
        // TODO: Validate email.
        if(StringUtils.isEmpty(getEmail())){
            throw new IllegalArgumentException("Email is required.");
        }

        if(StringUtils.isEmpty(getFullName())){
            throw new IllegalArgumentException("Name is required.");
        }
    }
}
