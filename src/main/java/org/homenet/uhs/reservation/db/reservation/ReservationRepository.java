package org.homenet.uhs.reservation.db.reservation;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

/**
 * @author uh on 2019/02/12.
 */
public interface ReservationRepository extends CrudRepository<Reservation, Long> {


    /**
     * Marks old reservations, everything before today, as expired.
     * TODO: We might want to delete or move very old ones later?
     */
    @Modifying
    @Query("update #{#entityName} e set e.expired = true where e.endDate < CURRENT_DATE()")
    @Transactional
    void markExpired();

    List<Reservation> findByUserIdAndExpiredFalse(Long userId);

    /**
     * Finds reservations overlapping exactly two dates.
     * @param startDate start date
     * @param endDate end date
     * @return reservation list
     */
    @Query("SELECT e FROM #{#entityName} e WHERE (:startDate between e.startDate AND e.endDate) " +
            "OR (:endDate between e.startDate AND e.endDate) " +
            "AND expired = false")
    List<Reservation> findOverlappingReservations(@Param("startDate") LocalDate startDate,
                                                  @Param("endDate") LocalDate endDate);

    /**
     * Returns all active reservations between two dates.
     * @param startDate start date
     * @param endDate end date
     * @return reservation list
     */
    List<Reservation> findByStartDateGreaterThanAndEndDateLessThanAndExpiredFalse(@Param("startDate") LocalDate startDate,
                                                                                  @Param("endDate") LocalDate endDate);
}
