package org.homenet.uhs.reservation.db.reservation;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.Period;

/**
 * @author uh on 2019/02/14.
 */
@Entity
public class Reservation {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private Long userId;

    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate startDate;

    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate endDate;

    /**
     * A reservation expires when the endDate is before
     */
    private boolean expired;

    protected Reservation(){
    }

    public Reservation(Long id, LocalDate startDate, LocalDate endDate) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + id +
                ", userId=" + userId +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", expired=" + expired +
                '}';
    }

    public void validateDateRange() {
        final LocalDate today = LocalDate.now();
        if(!(getStartDate() != null
                && getStartDate().isAfter(today)
                && getEndDate() != null
                && (getStartDate().isBefore(getEndDate()) || getStartDate().isEqual(getEndDate())))) {
            throw new IllegalArgumentException("Invalid date range, from '" + getStartDate() + "' to '" + getEndDate()
                    + "', both values must be present and 'from' must be before 'to'");
        }

        if(Period.between(getStartDate(), getEndDate()).getDays() > 3){
            throw new IllegalArgumentException("You cannot reserve more than " + 3 + " days.");
        }
    }

    public void validateAtLeastOneDayBeforeArrival() {
        final LocalDate today = LocalDate.now();

        if (!(getStartDate() != null && (today.isBefore(getStartDate().minusDays(1)) || today.isEqual(getStartDate().minusDays(1))))) {
            throw new IllegalArgumentException("The reservation must be made at least one day before the arrival date.");
        }

    }

    public void validateUpToOneMonthInAdvance(){
        final LocalDate today = LocalDate.now();

        if(!(getStartDate() != null && Period.between(today, getStartDate()).toTotalMonths() <= 1)){
            throw new IllegalArgumentException("The reservation can't be made more than one month in advance.");
        }
    }

    public void validate() {
        validateDateRange();
        validateAtLeastOneDayBeforeArrival();
        validateUpToOneMonthInAdvance();
    }
}
