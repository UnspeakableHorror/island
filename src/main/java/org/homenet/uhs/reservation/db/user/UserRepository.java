package org.homenet.uhs.reservation.db.user;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * @author uh on 2019/02/12.
 */
public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByEmail(String email);
}
