package org.homenet.uhs.reservation.web;

import org.homenet.uhs.reservation.service.ReservationCalendar;
import org.homenet.uhs.reservation.service.ReservationService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

/**
 * @author uh on 2019/02/11.
 */
@RequestMapping("camp")
@RestController
public class CampController {

    private ReservationService reservationService;

    public CampController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @GetMapping("availability/from/{from}/to/{to}")
    public List<ReservationCalendar.DayReserved> availability(
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
            @PathVariable LocalDate from,
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
            @PathVariable LocalDate to){

        return reservationService.availability(from, to).getCalendar();
    }

    @PostMapping("reserve")
    public ResponseEntity<Long> createReserve(@NotNull @Valid @RequestBody ReservationDTO reservation){
        return new ResponseEntity<>(reservationService.save(reservation), HttpStatus.ACCEPTED);
    }

    @PutMapping("reserve")
    public ResponseEntity<Long> updateReserve(@NotNull @Valid @RequestBody ReservationDTO reservation){

        if (reservation.getId() == null){
            throw new IllegalArgumentException("The id is required.");
        }

        return new ResponseEntity<>(reservationService.update(reservation), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("reserve/{reservationId}")
    public HttpStatus deleteReserve(@NotNull @PathVariable Long reservationId){
        reservationService.cancel(reservationId);
        return HttpStatus.ACCEPTED;
    }
}
