package org.homenet.uhs.reservation.service;

import org.homenet.uhs.reservation.db.reservation.Reservation;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Object that contains the logic to generate a lis with available dates to reserve.
 * @author uh on 2019/02/15.
 */
public class ReservationCalendar {

    private final LocalDate from;
    private final LocalDate to;
    private final List<LocalDate> calendar;
    private final List<Reservation> reservations;

    public ReservationCalendar(LocalDate from, LocalDate to, List<Reservation> reservations){
        this.from = from;
        this.to = to;
        this.calendar = from.datesUntil(to.plus(1, ChronoUnit.DAYS)).collect(Collectors.toList());
        this.reservations = reservations;
    }

    /**
     * Returns a list with dates availability.
     * @return list with dates.
     */
    public List<DayReserved> getCalendar(){
        //for each day mark as unavailable if there's a reservation.
        return calendar.stream()
                .map(date -> new DayReserved(date, isReserved(date, reservations)))
                .collect(Collectors.toList());

    }

    /**
     * Checks if a given date is available or not from a list of reservations.
     * @param date to check
     * @param reservations list of reservations.
     * @return if the date is reserved or not.
     */
    public boolean isReserved(final LocalDate date, final List<Reservation> reservations) {
        return reservations.stream()
                .anyMatch(r -> (date.isAfter(r.getStartDate()) || date.isEqual(r.getStartDate()))
                && (date.isBefore(r.getEndDate()) || date.isEqual(r.getEndDate())));
    }

    public LocalDate getFrom() {
        return from;
    }

    public LocalDate getTo() {
        return to;
    }

    // This class worked just fine without a public constructor and both variables as final
    // except on the integration test.
    public static class DayReserved {
        private LocalDate day;
        private boolean reserved;

        public DayReserved(){
        }

        public DayReserved(LocalDate day, boolean reserved) {
            this.day = day;
            this.reserved = reserved;
        }

        public LocalDate getDay() {
            return day;
        }

        public boolean isReserved() {
            return reserved;
        }
    }
}


