package org.homenet.uhs.reservation.service;

/**
 * @author uh on 2019/02/14.
 */
public class UserOverMaxDaysReservationsException extends IllegalArgumentException {
    public UserOverMaxDaysReservationsException(String message) {
        super(message);
    }
}
