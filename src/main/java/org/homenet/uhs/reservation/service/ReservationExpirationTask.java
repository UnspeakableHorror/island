package org.homenet.uhs.reservation.service;

import org.homenet.uhs.reservation.db.reservation.ReservationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Expires old reservations but keeps in them the database.
 */
@Service
public class ReservationExpirationTask {
    private static final Logger log = LoggerFactory.getLogger(ReservationExpirationTask.class);

    private final ReservationRepository reservationRepository;

    public ReservationExpirationTask(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    /**
     * Expires old reservations after starting up the application.
     */
    @PostConstruct
    public void init(){
        expireOldReservations();
    }

    @Scheduled(cron = "${cron.task.expire.reservations}")
    public void expireOldReservations(){
        log.info("Expiring old reservations.");

        reservationRepository.markExpired();
    }
}
