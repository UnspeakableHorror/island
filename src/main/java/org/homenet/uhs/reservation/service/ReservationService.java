package org.homenet.uhs.reservation.service;

import org.homenet.uhs.reservation.web.ReservationDTO;

import java.time.LocalDate;

/**
 * @author uh on 2019/02/12.
 */
public interface ReservationService {

    ReservationCalendar availability(LocalDate from, LocalDate to);

    Long save(ReservationDTO reservation);

    void cancel(Long reservationId);

    Long update(ReservationDTO reservation);
}
