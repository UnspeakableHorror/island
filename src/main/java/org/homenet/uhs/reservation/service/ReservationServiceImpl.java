package org.homenet.uhs.reservation.service;

import org.homenet.uhs.configuration.ReservationConfiguration;
import org.homenet.uhs.reservation.db.reservation.Reservation;
import org.homenet.uhs.reservation.db.reservation.ReservationRepository;
import org.homenet.uhs.reservation.db.user.User;
import org.homenet.uhs.reservation.db.user.UserRepository;
import org.homenet.uhs.reservation.web.ReservationDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

/**
 * Handles reservations queries.
 *
 * @author uh on 2019/02/12.
 */
@Service
public class ReservationServiceImpl implements ReservationService {

    private final ReservationConfiguration reservationConfiguration;
    private final UserRepository userRepository;
    private final ReservationRepository reservationRepository;

    public ReservationServiceImpl(ReservationConfiguration reservationConfiguration,
                                  UserRepository userRepository,
                                  ReservationRepository reservationRepository) {
        this.reservationConfiguration = reservationConfiguration;
        this.userRepository = userRepository;
        this.reservationRepository = reservationRepository;
    }

    @Override
    public ReservationCalendar availability(LocalDate from, LocalDate to) {

        if(from.isAfter(to)){
            throw new IllegalArgumentException("Date 'from' -> '" + from + "' is after date 'to' -> '" + to + "'.");
        }

        return new ReservationCalendar(from, to, reservationRepository.findByStartDateGreaterThanAndEndDateLessThanAndExpiredFalse(from, to));
    }

    @Override
    @Transactional
    public Long save(@NotNull ReservationDTO reservationDTO) {
        final var reservation = toReservation(reservationDTO);
        final var user = toUser(reservationDTO);

        // if the user already exists check that all other reservations are already expired
        // and he has enough days left to spend.
        // add it to the reservation.
        // save the reservation.

        // if the user does not exist, create it,
        // set it in the reservation
        // save both.

        final var foundUser = userRepository.findByEmail(user.getEmail());

        if (foundUser.isPresent()){
            checkAvailableDays(user, reservation, reservationRepository.findByUserIdAndExpiredFalse(user.getId()));
            checkAnyReservationDatesOverlap(reservation);

            reservation.setUserId(foundUser.get().getId());
            return reservationRepository.save(reservation).getId();
        } else {
            checkAnyReservationDatesOverlap(reservation);

            userRepository.save(user);

            reservation.setUserId(user.getId());
            return reservationRepository.save(reservation).getId();
        }
    }

    @Override
    @Transactional
    public Long update(@NotNull ReservationDTO reservationDTO) {

        final var foundReservation = reservationRepository.findById(reservationDTO.getId());
        final var foundUser = userRepository.findByEmail(reservationDTO.getEmail());

        if(foundReservation.isEmpty()){
            throw new NoSuchElementException("Reservation id: '" + reservationDTO.getId() + "' could not be found.");
        }

        if(foundUser.isEmpty()){
            throw new NoSuchElementException("User: '" + reservationDTO.getEmail() + "' could not be found.");
        }

        final var reservation = foundReservation.get();

        reservation.setStartDate(reservationDTO.getStartDate());
        reservation.setEndDate(reservationDTO.getEndDate());

        // check that the user editing the reservation is the same that the one that created it.
        if(!reservation.getUserId().equals(foundUser.get().getId())) {
            throw new IllegalAccessException("Not allowed.");
        }

        checkAvailableDays(foundUser.get(), reservation, reservationRepository.findByUserIdAndExpiredFalse(foundUser.get().getId()));
        checkAnyReservationDatesOverlap(reservation);

        // update the name if changed
        if(!foundUser.get().getFullName().equalsIgnoreCase(reservationDTO.getFullName())) {
            foundUser.get().setFullName(reservationDTO.getFullName());
            userRepository.save(foundUser.get());
        }

        return reservationRepository.save(reservation).getId();
    }

    /**
     * Sums the number of days of all reservations adding the number of days from the new reservation
     * and compares against the configured maximum number of days, if it'sgreater throws UserOverMaxDaysReservationsException.
     * @param user the user
     * @param reservation the reservation.
     * @param reservations all the user active reservations
     * @throws UserOverMaxDaysReservationsException if the user already has reservations and has no days available.
     */
    private void checkAvailableDays(User user, Reservation reservation, List<Reservation> reservations)
            throws UserOverMaxDaysReservationsException {
        final var userDays = reservations.stream()
                .mapToInt(r -> Period.between(r.getStartDate(), r.getEndDate()).getDays())
                .sum()
                + Period.between(reservation.getStartDate(), reservation.getEndDate()).getDays();

        if(!reservations.isEmpty()
                && userDays > reservationConfiguration.getMaxDays()){

            var message = "The total number of days for user '" +
                    user.getEmail() +
                    "'" +
                    " add up to '" +
                    userDays +
                    "'. " +
                    " Which is above the maximum number of days/user of '" +
                    reservationConfiguration.getMaxDays() +
                    "'. " +
                    "Either modify the requested reservation, an existing one or cancel other reservations.";
            throw new UserOverMaxDaysReservationsException(message);
        }
    }

    /**
     * Check that there are no other reservations with the same start date or end date.
     * @param reservation reservation to check with.
     */
    private void checkAnyReservationDatesOverlap(Reservation reservation) {
        List<Reservation> reservations = reservationRepository
                .findOverlappingReservations(reservation.getStartDate(), reservation.getEndDate())
                .stream().filter(it -> !it.getId().equals(reservation.getId()))
                .collect(Collectors.toList());

        if(!reservations.isEmpty()) {
            throw new IllegalArgumentException("Reservations matching or partially matching the same period already exist. ");
        }
    }

    /**
     * Cancels a reservation. <br/>
     * TODO: Currently deletes it, we might want to keep historical data so
     * maybe it would be better to just mark them as expired.
     * @param reservationId the reservation id to cancel.
     */
    @Override
    public void cancel(Long reservationId) {
        reservationRepository.deleteById(reservationId);
    }

    private User toUser(ReservationDTO reservationDTO) {
        return new User(reservationDTO.getEmail(), reservationDTO.getFullName());
    }

    private Reservation toReservation(ReservationDTO reservationDTO) {
        var reservation = new Reservation(reservationDTO.getId(), reservationDTO.getStartDate(), reservationDTO.getEndDate());
        reservation.validate();

        return reservation;
    }
}
