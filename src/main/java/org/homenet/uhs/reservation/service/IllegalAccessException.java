package org.homenet.uhs.reservation.service;

/**
 * @author uh on 2019/02/15.
 */
public class IllegalAccessException extends RuntimeException {
    public IllegalAccessException(String message){
        super(message);
    }
}
